## syntax=docker/dockerfile:1

#ARG NODE_VERSION=20.10.0

#FROM node:${NODE_VERSION}-alpine

#ENV NODE_ENV production
#ENV NUMBER=-1

#WORKDIR .

#COPY package*.json .
#RUN npm install

#RUN --mount=type=bind,source=package.json,target=package.json \
#    --mount=type=bind,source=package-lock.json,target=package-lock.json \
#    --mount=type=cache,target=/root/.npm \
#    npm ci --omit=dev

#USER node

#COPY . ./app

#EXPOSE 8080

#CMD npm run start:server

ARG NODE_VERSION=20.10.0

FROM node:${NODE_VERSION}-alpine
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
EXPOSE 8080
CMD npm run start:server