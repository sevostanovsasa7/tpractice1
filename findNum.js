// Digest: sha256:938a20776072c5ca0d42ebb5e437f27b7dcbd000fa0290e572d6194e35ad40d9

//const args = process.argv.slice(2);
//const arraySorted = Array.from({ length: 100 }, () =>
//  Math.floor(Math.random() * 100)
//).sort((a, b) => a - b);

//function findNumber(array, number) {
//  if (!number) {
//    console.log("No arguments");
//  }

//  return binarySearch(array, number);
//}

function binarySearch(arr, val) {
  const findNumber = Number(val);
  let start = 0;
  let end = arr.length - 1;

  while (start <= end) {
    let middle = Math.floor((start + end) / 2);

    if (arr[middle] === findNumber) {
      return middle;
    } else if (arr[middle] < findNumber) {
      start = middle + 1;
    } else {
      end = middle - 1;
    }
  }
  return -1;
}

// 36
//const result = findNumber(arraySorted, Number(args[0]));
//console.log(result);

module.exports = binarySearch;
