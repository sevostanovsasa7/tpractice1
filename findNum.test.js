// binarySearch.test.js
const binarySearch = require("./findNum");

test("binary search returns the correct index", () => {
  expect(binarySearch([1, 3, 5, 7, 9], 5)).toBe(2);
  expect(binarySearch([1, 3, 5, 7, 9], 9)).toBe(4);
});

test("binary search returns -1 for value not found", () => {
  expect(binarySearch([1, 3, 5, 7, 9], 2)).toBe(-1);
});
