const express = require("express");
let { createClient } = require("redis");
const binarySearch = require("./findNum");

const PORT = 8080;

const app = express();
const client = createClient({
  url: "redis://redis:6379",
});

let mainArray = [];

app.get("/", async (req, res) => {
  return res.json(mainArray);
});

app.get("/findNumber", async (req, res) => {
  const { number } = req.query;
  const index = binarySearch(mainArray, number);
  return res.json({ index });
});

async function start() {
  client.on("error", (err) => console.log("Redis Client Error:", err));
  await client.connect();

  app.listen(PORT, () => {
    console.log(`Приложение запущено на ${PORT}`);
  });

  (async () => {
    const savedArray = await client.get("array");
    console.log("Saved array", savedArray);
    if (savedArray) {
      mainArray = JSON.parse(savedArray);
      return;
    }
    mainArray = Array.from({ length: 100 }, () =>
      Math.floor(Math.random() * 200)
    ).sort((a, b) => a - b);
    await client.set("array", JSON.stringify(mainArray));
  })();
}

start();
